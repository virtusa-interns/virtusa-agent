from flask import Flask, render_template, request, jsonify

from chat import get_response


app = Flask(__name__)

@app.route("/", methods=["GET"] )
def index_get():
    return render_template("base.html")

@app.route("/predict", methods=["POST"])
def predict():
    text = request.get_json().get("message")
    response=get_response(text, email="bob@gmail.com", mobileNumber="9866655233")
    message= {"answer": response}
    return jsonify(message)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8080)

